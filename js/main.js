// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyCLOvKeuJ3SMqzW2d6dn-4ETTXAm1hIfqQ",
    authDomain: "miproyecto-bc292.firebaseapp.com",
    databaseURL: "https://miproyecto-bc292-default-rtdb.firebaseio.com",
    projectId: "miproyecto-bc292",
    storageBucket: "miproyecto-bc292.appspot.com",
    messagingSenderId: "1058621180199",
    appId: "1:1058621180199:web:20be70e889b66879d68102"
  };

  // Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage= getStorage();

//variables para manejar la imgen

const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progreesDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');


//Funciones
function leerInputs() {
    const Serie = document.getElementById('txtSerie').value;
    const marca = document.getElementById('txtMarca').value;
    const modelo = document.getElementById('txtModelo').value;
    const informacion = document.getElementById('txtDescripcion').value;
    const urlImag = document.getElementById('txtUrl').value;

    return { Serie, marca, modelo, informacion, urlImag };
}

function mostrarMensaje(mensaje) {
    const mensajeElement = document.getElementById('mensaje');
    mensajeElement.textContent = mensaje;
    mensajeElement.style.display = 'block';
    setTimeout(() => { mensajeElement.style.display = 'none' }, 1000);
}

//Agregar productos a la DB
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);

function insertarProducto() {
    const { Serie, marca, modelo, informacion, urlImag } = leerInputs();
    //validar
    if (Serie === "" || marca === "" || modelo === "" || informacion === "") {
        mostrarMensaje("Faltaron Datos por Capturar");
        return;
    }
    set(
        refS(db, 'Automoviles/' + Serie),
        {
            Serie: Serie,
            marca: marca,
            modelo: modelo,
            informacion: informacion,
            urlImag: urlImag
        }
    ).then(() => {
        alert("Se agrego con exito");
    }).catch((error) => {
        alert("Ocurrio un error :(")
    })
}

function limpiarInputs() {
    document.getElementById('txtSerie').value = '';
    document.getElementById('txtModelo').value = '';
    document.getElementById('txtMarca').value = '';
    document.getElementById('txtDescripcion').value = '';
    document.getElementById('txtUrl').value = '';
}

function escribirInputs(Serie, marca, modelo, informacion, urlImag) {
    document.getElementById('txtSerie').value = Serie;
    document.getElementById('txtModelo').value = modelo;
    document.getElementById('txtMarca').value = marca;
    document.getElementById('txtDescripcion').value = informacion;
    document.getElementById('txtUrl').value = urlImag;
}

function buscarProducto() {
    const Serie = document.getElementById('txtSerie').value.trim();
    if (Serie === "") {
        mostrarMensaje("No se ingreso  Serie");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Automoviles/' + Serie)).then((snapshot) => {
        if (snapshot.exists()) {
            const { marca, modelo, informacion, urlImag } = snapshot.val();
            escribirInputs(Serie, marca, modelo, informacion, urlImag);
        } else {
            limpiarInputs();
            mostrarMensaje("El producto con codigo " + Serie + "No Existe:(");
        }
    })
}

const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', buscarProducto);

//Listar Productos

function Listarproductos() {
    const dbref = refS(db, 'Automoviles');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';
    onValue(dbref, (snapshot) => {
        snapshot.forEach(childSnapshot => {
            const childKey = childSnapshot.key;
            const data = childSnapshot.val();
            var fila = document.createElement('tr');

            var celdaCodigo = document.createElement('td');
            celdaCodigo.textContent = childKey;
            fila.appendChild(celdaCodigo);

            var celdaNombre = document.createElement('td');
            celdaNombre.textContent = data.marca;
            fila.appendChild(celdaNombre);

            var celdaPrecio = document.createElement('td');
            celdaPrecio.textContent = data.modelo;
            fila.appendChild(celdaPrecio);

            var celdaCantidad = document.createElement('td');
            celdaCantidad.textContent = data.informacion;
            fila.appendChild(celdaCantidad);

            var celdaImagen = document.createElement('td');
            var imagen = document.createElement('img');
            imagen.src = data.urlImag;
            imagen.width = 100;
            celdaImagen.appendChild(imagen);
            fila.appendChild(celdaImagen);
            tbody.appendChild(fila);
        });
    }, { onlyOnce: true });
}

Listarproductos();

//Funcion actualizar

function actualizarAutomovil() {
    const { Serie, marca, modelo, informacion, urlImag } = leerInputs();
    if (Serie === "" || marca === "" || modelo === "" || informacion === "") {
        mostrarMensaje("Favor de capturar toda la informacion");
        return;
    }
    update(refS(db, 'Automoviles/' + Serie), {
        Serie: Serie,
        marca: marca,
        modelo: modelo,
        informacion: informacion,
        urlImag: urlImag
    }).then(() => {
        mostrarMensaje("Se actualizo con exito");
        limpiarInputs();
        Listarproductos();
    }).catch((error) => {
        mostrarMensaje("Ocurrio un error: " + error);
    });
}

const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarAutomovil);

//Funcion Borrar

function eliminarAutomovil() {
    const Serie = document.getElementById('txtSerie').value.trim();
    if (Serie === "") {
        mostrarMensaje("No se ingreso un Codigo Valido.");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Automoviles/' + Serie)).then((snapshot) => {
        if (snapshot.exists()) {
            remove(refS(db, 'Automoviles/' + Serie)).then(() => {
                mostrarMensaje("Producto eliminado con éxito.");
                limpiarInputs();
                Listarproductos();
            }).catch((error) => {
                mostrarMensaje("Ocurrio un error al eliminar el producto: " + error);
            });
        } else {
            limpiarInputs();
            mostrarMensaje("El producto con ID " + Serie + " no existe.");
        }
    });
}

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarAutomovil);


document.addEventListener('DOMContentLoaded', function () {
    const imageInput = document.getElementById('imageInput');
    const uploadButton = document.getElementById('uploadButton');
    const imageContainer = document.getElementById('imageContainer');

    uploadButton.addEventListener('click', function () {
        const urlImag = imageInput.value;

        if (urlImag) {
            const img = document.createElement('img');
            img.src = urlImag;
            img.alt = 'Imagen subida por el usuario';
            img.classList.add('mi-clase-de-imagen');
            imageContainer.innerHTML = '';
            imageContainer.appendChild(img);
        } else {
            alert('Por favor, ingresa una URL de imagen.');
        }
    });
});

uploadButton.addEventListener('click', (Event) =>{
    Event.preventDefault();
    const file = imageInput.files[0];

    if(file) { 
        const storageRef = ref(storage, file.name);
        const uploadTask = uploadBytesResumable(storageRef,file);
        uploadTask.on('state_changed', (snapshot) =>{
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            progreesDiv.textContent = 'progreso: ' + progress.toFixed(2) + '%';
            }, (error) =>{
                console.error(error);
            }, () => { 
                getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                    txtUrlInput.value = downloadURL;
                    setTimeout(() => {
                        progreesDiv.textContent = '';
                    }, 500);
                }).catch((error) => {
                    console.error(Error);
                });
            });
        }
});


