// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyCLOvKeuJ3SMqzW2d6dn-4ETTXAm1hIfqQ",
    authDomain: "miproyecto-bc292.firebaseapp.com",
    databaseURL: "https://miproyecto-bc292-default-rtdb.firebaseio.com",
    projectId: "miproyecto-bc292",
    storageBucket: "miproyecto-bc292.appspot.com",
    messagingSenderId: "1058621180199",
    appId: "1:1058621180199:web:20be70e889b66879d68102"
  };

  // Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage= getStorage();

  // declarar unas variables global
  var Serie =0;
  var marca ="";
  var modelo = "";
  var informacion = "";
  var urlImag  = ""
  
  
  
  
  // listar productos
  Listarproductos(marca)
  function Listarproductos() {
      const titulo= document.getElementById("titulo");
      const  section = document.getElementById('secArticulos')
      titulo.innerHTML= marca; 
      
      
    const dbRef = refS(db, 'Automoviles');
    const tabla = document.getElementById('tablaProductos');
   
    onValue(dbRef, (snapshot) => {
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
  
            const data = childSnapshot.val();
           
  
          
  
            section.innerHTML+= "<div class ='card'> " +
                       "<img  id='urlImag' src=' "+ data.urlImag + "'  alt=''  width='200'> "
                       + "<h1 id='marca'  class='title'>" + data.marca+ ":" + data.modelo +"</h1> "
                       + "<p  id='descripcion' class ='anta-regurar'>" + data.informacion
                       +  "</p> <button> Mas Informacion </button> </div>"  ;           
  
  
        });
    }, { onlyOnce: true });
  }
  
  